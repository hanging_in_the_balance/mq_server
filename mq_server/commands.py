_COMMANDS = {}


def register(key):
    def decorated(command):
        _COMMANDS[key] = command

        def wrapper(*args, **kwargs):
            return command(*args, **kwargs)
        return wrapper
    return decorated


class CommandException(Exception):
    pass


def get_command(key):
    command = _COMMANDS.get(key)
    if not command:
        raise CommandException('unknown command')
    return command
