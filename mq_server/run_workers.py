#!/usr/bin/env python
import os
import subprocess
import sys


ROOT = os.path.dirname(os.path.abspath(__file__))
WORKER_PATH = os.path.join(ROOT, 'worker.py')

workers_count = int(sys.argv[1]) if len(sys.argv) > 1 else 1

for _i in range(workers_count):
    subprocess.Popen([WORKER_PATH], stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE)
print('ok!')