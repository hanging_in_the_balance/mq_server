CONNECTION_PARAMETERS = {
    'host': 'localhost',
    'port': 5672
}
QUEUE_NAME = 'abstract_service'
QUEUE_ROUTING_KEY = 'abstract_service'
QUEUE_CONSUMER_TAG = 'abstract_service_consumer'

API_URI = 'http://127.0.0.1:8000'

INSTALLED_COMMANDS = (
    'example_commands',
)