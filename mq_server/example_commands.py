import time

from commands import register


@register('fibonacci')
def fibonacci(n):
    print " [x] start fibonacci"
    try:
        n = int(n)
        if n < 0:
            raise ValueError
        result = int((((1.0 + 5 ** .5) / 2.0) ** n - ((1.0 - 5 ** .5) / 2.0) ** n) / (5 ** .5))
        return 'success', result
    except ValueError as e:
        return 'error', 'wrong body'


@register('sleep')
def sleep(n):
    print " [x] start sleep"
    try:
        n = int(n)
        time.sleep(n)
        return 'success', 'good morning!'
    except ValueError as e:
        return 'error', 'wrong body'


@register('echo')
def echo(s):
    print " [x] start echo"
    return 'success', s
