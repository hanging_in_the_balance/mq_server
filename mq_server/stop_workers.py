#!/usr/bin/env python
import subprocess


process = subprocess.Popen(['rabbitmqctl', 'list_connections', 'pid'],
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE)

out, err = process.communicate()
if out.startswith('{error'):
    print(out)
    exit()

connections = out.splitlines()[1:-1]

for connection in connections:
    subprocess.call(['rabbitmqctl', 'close_connection',
                     connection, '"bye"'])
print('ok!')