#!/usr/bin/env python
import pika
import requests

from commands import get_command, CommandException
import settings


def _import_commands():
    map(__import__, settings.INSTALLED_COMMANDS)
_import_commands()


class Worker:
    def __init__(self, connection_parameters, queue_name):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(**connection_parameters)
        )
        self.channel = self.connection.channel()
        self.queue_name = queue_name
        self.channel.queue_declare(queue=queue_name, durable=True)

    def run(self):
        print ' [*] Waiting for messages. To exit press CTRL+C'
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(Worker._callback, queue=self.queue_name,
                                   consumer_tag=settings.QUEUE_CONSUMER_TAG)
        self.channel.start_consuming()

    @staticmethod
    def _callback(ch, method, properties, body):
        print " [x] Received %r" % (body,)
        try:
            message_id, command, param = body.split(':')
            result = get_command(command)(param)
        except CommandException as e:
            result = 'error', str(e)
            message_id = 0
        except ValueError:
            result = 'error', 'wrong body'
            message_id = 0
        print " [x] {}".format(result[0])
        response = requests.post('{}/tasks/message/{}/'.format(settings.API_URI, message_id), data={
            'status': result[0],
            'result': result[1]
        })
        print response.content
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    Worker(settings.CONNECTION_PARAMETERS, settings.QUEUE_NAME).run()