## Установка ##
[Установите RabbitMQ](http://www.rabbitmq.com/download.html), склонируйте репозиторий и установите зависимости в virtualenv:
```
#!bash
$ virtualenv mq_server_venv --no-site-packages  # создаем виртуальное окружение
$ source mq_server_venv/bin/activate  # активируем виртуальное окружение

$ git clone git@bitbucket.org:hanging_in_the_balance/mq_server.git
$ cd mq_server
$ pip install -r requirements/base.txt  # устанавливаем зависимости

```


## Использование ##
Для запуска воркеров в фоновом режиме используйте скрипт run_workers.py:
```
#!bash
$ ./run_workers.py  # запустить 1 воркер
ok!
$ ./run_workers.py 20  # запустить 20 воркеров
ok!
```

Для остановки воркеров используйте stop_workers.py:
```
#!bash
$ sudo ./stop_workers.py
Closing connection "<rabbit@hanging.3.9886.0>" ...
...done.
Closing connection "<rabbit@hanging.3.9902.0>" ...
...done.
...
Closing connection "<rabbit@hanging.3.9913.0>" ...
...done.
ok!
```

## Формат запросов ##
Воркеры принимают сообщения вида 'идентификатор_сообщения:команда:параметр'. Пример: '54:sleep:15'.


## Добавление новых команд ##
Мы легко можем добавлять новые команды для воркеров. Для этого нам нужно создать функцию, которая принимает один 
аргумент и возвращает кортеж, первым элементом которого является статус, а вторым - результат. 

Находясь в одной папке с файлом worker.py, создадим новый модуль для нашей команды:
```
#!bash
$ touch awesome_commands.py
```

В этом модуле создадим простую функцию, принимающую один аргумент, и обернем её в декоратор @register:
```
#!python
from commands import register


@register('add5')
def addfive(n):
    try:
        n = int(n)        
        return 'success', n + 5
    except ValueError as e:
        return 'error', 'wrong body'
```

Осталось только добавить наш модуль в settings.INSTALLED_COMMANDS:
```
#!python
...
INSTALLED_COMMANDS = (
    'example_commands',
    'awesome_commands',
)
```

Все. Теперь воркеры могут принимать запросы с командой 'add5'. 